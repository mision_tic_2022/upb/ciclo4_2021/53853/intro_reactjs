
const MiPrimerComponente = () => {
    return (
        <div>
            <h2>Mi Primer Componente</h2>
            <h3>Subtitulo desde Mi Primer Componente</h3>
            <p>kjuytjbvmnvguytuytrasdasd qweuytuiytqwe </p>
            {/**Formulario de registro*/}
            <form>
                <label htmlFor="nombre">Nombre: </label>
                <input type="text" placeholder="Nombre" name="nombre" id="nombre"/>
                <br/>
                <label htmlFor="apellido">Apellido: </label>
                <input type="text" placeholder="Apellido" name="apellido" id="apellido"/>
                <br/>
                <label htmlFor="email">Email: </label>
                <input type="email" placeholder="Email" name="email" id="email"/>
            </form>
        </div>
    );
}

export default MiPrimerComponente;