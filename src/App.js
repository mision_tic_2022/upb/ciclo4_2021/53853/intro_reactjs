import logo from './logo.svg';
import './App.css';
import MiPrimerComponente from './components/MiPrimerComponente';
import ComponenteConEstado from './components/ComponenteConEstado';

function App() {

  const titulo = <h2>Hola mundo</h2>;
  const universidad = <h3>Misión Tic 2022 UPB</h3>

  return (
    <div className="App">
      <header className="App-header">
        {titulo}
        {universidad}
        <img src={logo} className="App-logo" alt="logo" />        
        {/*--------Esto es un comentario---------*/}
        <ComponenteConEstado/>
        <MiPrimerComponente/>
      </header>
    </div>
  );
}

export default App;
